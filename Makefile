SHELL := /bin/bash -x
NET_NAME = workspace
PATH_WORK=$(shell pwd)
NETWORK=$(shell docker network ls | grep ${NET_NAME} > /dev/null)

PHP_BUILD ?= 7.0
PHP_NAME ?= 70

PORT ?= 9000

VAL=$(shell docker inspect --format '{{ .NetworkSettings.Networks.${NET_NAME}.IPAddress }}' Web_services)

setup:
ifndef NETWORK
	echo 'Your network exsits'
else
	docker network create --internal ${NET_NAME}
endif

start:
	cd ./docker/ && docker-compose up -d

build:
	docker build --pull --build-arg PHP_VERSION=${PHP_BUILD} -t local-php:${PHP_BUILD} ./docker
	docker run -d --network ${NET_NAME} --name=php${PHP_NAME} \
	-v ${PATH_WORK}/projects:/var/www \
	-v ${PATH_WORK}/data/hosts:/etc/hosts \
	-w /var/www local-php:${PHP_BUILD}

redis:
	docker run -d --network ${NET_NAME} --restart=always --name=redis redis

php:
	docker run -d --network ${NET_NAME} --name=php${PHP_NAME} \
	-v ${PATH_WORK}/projects:/var/www \
	-v ${PATH_WORK}/data/hosts:/etc/hosts \
	-w /var/www php:${PHP_BUILD}-fpm 

down:
	cd ./docker/ && docker-compose down

check:
	cd ./docker/ && docker-compose exec -T web /data/wait-for-it.sh -h ${HOST} -p ${PORT}

composer:
	cd ./docker/ && docker-compose exec -T php${PHP_NAME} bash -c "cd /var/www/${PROJECT_NAME} && composer ${OPERATION}"

ip: 
	@docker inspect --format '{{ .NetworkSettings.Networks.${NET_NAME}.IPAddress }}' Web_services

add:
	echo "${VAL} ${SERVER_NAME}" >> ${PATH_WORK}/data/hosts