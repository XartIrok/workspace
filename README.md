# Workspace
Start using
```
apt-get install -yq make && make setup
```

# Extend connetion between container
```
make add SERVER_NAME=php73.local
```
This command extend dynamic shared `/etc/hosts` file

# Test scoope
Build diffrent php image

```
make build PHP_BUILD=7.2 PHP_NAME=72
```

You can check connection between web container with order container

```
make check HOST=php72 PORT=9000
```

# Wildcards local for windows 10
1. Install [Acrylic DNS Proxy](https://mayakron.altervista.org/) 
2. Edit network card, [how to do it](https://mayakron.altervista.org/support/acrylic/Windows10Configuration.htm)
3. Next edit config `AcrylicConfiguration.ini` change port in file 
```
PrimaryServerPort=54
```
4. Next edit file `AcrylicHosts.txt` on end file add this:
```
127.0.0.1   *.localhost
127.0.0.1   *.local
127.0.0.1   *.lc
```
5. Active DNS

# Problems?
If you have any problems try lunch command with `sudo`

# TODO
- [ ] certificate
- [ ] better optimize for build images
- [x] dynamic links between container
- [x] quick extend with new php container